package ru.detmir.tests;

import com.codeborne.selenide.Condition;
import org.assertj.core.api.Assertions;
import org.testng.annotations.Test;
import ru.detmir.framework.CartPage;
import ru.detmir.framework.ProductPage;

import java.util.List;

import static com.codeborne.selenide.Selenide.$;

public class ProductTest extends ProductPage {

    /**
     * Положитиь товар в корзину
     * Проверить, что кнопка добавления изменилась (товар добавлен)
     * Проверить через API, что товар добавился в корзину
     * Удалиить товар из корзины через UI
     * Проверить через API, что в корзине нет товаров
     */
    @Test
    public void test() {
        String productId = "3228444";

        open(productId);
        addProductToCart();
        checkAddedProduct();

        CartPage cartPage = new CartPage();
        addProductToCart();

        List<String> productsIdsFromApi = cartPage.getProductsIdsFromApi();
        Assertions.assertThat(productsIdsFromApi).containsExactly(productId);

        cartPage.removeProduct(0);
        List<String> productsIdsFromApiAfterRemove = cartPage.getProductsIdsFromApi();
        Assertions.assertThat(productsIdsFromApiAfterRemove).isEmpty();
    }

    private void checkAddedProduct() {
        $("#addButton").shouldHave(Condition.text("В корзине"));
    }

}