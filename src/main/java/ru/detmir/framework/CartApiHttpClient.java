package ru.detmir.framework;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import java.util.List;

public class CartApiHttpClient {

    public List<String> getProductsIdsFromApi(String session) {
        Response response = RestAssured.given().cookie("session", session)
                .baseUri("https://api.detmir.ru")
                .basePath("/v2/cart")
                .get();

        List<String> productIds = response.jsonPath().getList("items.id", String.class);

        return productIds;
    }
}
