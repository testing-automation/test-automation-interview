package ru.detmir.framework;

import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.Cookie;

import java.util.List;

import static com.codeborne.selenide.Selenide.$$;

public class CartPage {

    private CartApiHttpClient cartApiHttpClient = new CartApiHttpClient();

    public List<String> getProductsIdsFromApi() {
        Cookie cookie = WebDriverRunner.getWebDriver().manage().getCookieNamed("session-id");

        List<String> productsIdsFromApi = cartApiHttpClient.getProductsIdsFromApi(cookie.getValue());

        return productsIdsFromApi;
    }

    public void removeProduct(int position) {
        $$("#remove-product").get(position).click();
    }
}
