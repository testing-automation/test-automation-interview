package ru.detmir.framework;

import com.codeborne.selenide.Selenide;

import static com.codeborne.selenide.Selectors.byText;

public class ProductPage {

    public void open(String string) {
        Selenide.open("https://www.detmir.ru/product/index/id/" + string);
    }

    public void addProductToCart() {
        Selenide.$(byText("В корзину")).click();
    }


}